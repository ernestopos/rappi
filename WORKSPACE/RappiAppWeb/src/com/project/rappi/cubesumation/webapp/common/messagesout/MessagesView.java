package com.project.rappi.cubesumation.webapp.common.messagesout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

public class MessagesView {
	public static void info(String update,String detail) {
		FacesContext.getCurrentInstance().addMessage(update,new FacesMessage(FacesMessage.SEVERITY_INFO, "Información: "+ detail, detail));		
	}

	public static void warn(String detail) {
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_WARN, "Precaución: " + detail, detail));
	}

	public static void error(String detail) {
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: " + detail, detail));
	}

	public static void fatal(String detail) {
		FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error fatal: ", detail));
	}

	public static void error(List<String> messages) {
		for (String m : messages) {
			FacesContext.getCurrentInstance().addMessage(m,new FacesMessage(FacesMessage.SEVERITY_ERROR, m,m));
		}
	}
	public void showMessage(String detail) {
		Map<String,Object> options = new HashMap<String, Object>();
        options.put("modal", true);         
        RequestContext.getCurrentInstance().openDialog(detail, options, null);        
    }
}
