function borrarImportacion(){
    document.getElementById('form1:itAnioDeclaracionImp').value = "";
    document.getElementById('form1:itValorUSDImpo').value = "";
    document.getElementById('form1:itNumeralCambiarioDesOp').value = "";
    document.getElementById('form1:itFechaDocTrans').value = "";
}

function borrarImportacionInformeDatosFaltantes(){
    document.getElementById('form1:itNumeroImportacion').value = "";
    document.getElementById('form1:itAnioImportacion').value = "";
    document.getElementById('form1:itValorUSDImpo').value = "";
    document.getElementById('form1:itFechaDocTrans').value = "";
    document.getElementById('form1:itNumeroDocTrans').value = "";

}

function borrarExportacion(){
    document.getElementById('form1:itFechaDEX').value = "";
    document.getElementById('form1:itValorUSDExpo').value = "";
    document.getElementById('form1:itNumeralCambiarioExpo').value = "";
}

function borrarExportacionInformeDatosFaltantes(){
    document.getElementById('form1:itNumeroIExportacion').value = "";
    document.getElementById('form1:itFechaExportacion').value = "";
    document.getElementById('form1:itValorUSDExportacion').value = "";
    document.getElementById('form1:itNumeralExportacion').value = "";
    document.getElementById('form1:itTotalGastos').value = "";
    document.getElementById('form1:itDeducciones').value = "";
}

function borrarDeclaracionTres(){
    document.getElementById('form1:itNumeralTres').value = "";
    document.getElementById('form1:itValorUSDTres').value = "";
}

function borrarDeclaracionCinco(){
    document.getElementById('form1:itNumeralCinco').value = "";
    document.getElementById('form1:itValorUSDCinco').value = "";
}

function borrarDeclImpoF1(){
    document.getElementById('form:itAnioDeclaracionImp').value = "";
    document.getElementById('form:itNumeroDeclaracionImp').value = "";
    document.getElementById('form:itValorUSDImpo').value = "";
}

function borrarOperacionF1(){
    document.getElementById('form:itTipoCambioUSDDesOp').value = "";
    document.getElementById('form:itNumeralCambiarioDesOp').value = "";
    document.getElementById('form:itValorMonedaGiroDesOp').value = "";
}

function borrarDocTransF1(){
    document.getElementById('form:itFechaDocTrans').value = "";
    document.getElementById('form:itNumeroDocTrans').value = "";
}

function borrarDeclExpoF2(){
    document.getElementById('form:itFechaDeclaracionExp').value = "";
    document.getElementById('form:itCiudadAduanaDecExp').value = "";
    document.getElementById('form:itNumeralDeclaracionExp').value = "";
    document.getElementById('form:itValorReintegroUSDDecExp').value = "";
    document.getElementById('form:itNumeroDeclaracionExp').value = "";

}

function borrarNumF3(){
    document.getElementById('form:itNumeral').value = "";
    document.getElementById('form:itValorMonedaN').value = "";
    document.getElementById('form:itValorMonedaC').value = "";
    document.getElementById('form:itValorUSD').value = "";

}


function borrarLiqF3(){
    document.getElementById('form:itBaseMonedaContratadaLiq').value = "";
    document.getElementById('form:calFechaInicioliq').value = "";
    document.getElementById('form:calFechaFinalLiq').value = "";
    document.getElementById('form:itDiasLiq').value = "";
    document.getElementById('form:itTasaLiq').value = "";
}


function borrarDeclExpoF3(){
    document.getElementById('form:itNumeroDecExp').value = "";
    document.getElementById('form:sidFechaDecExp').value = "";
    document.getElementById('form:itCodigoCiudadDecExp').value = "";
    document.getElementById('form:itNumeralDecExp').value = "";
    document.getElementById('form:itValorReintegroDecExp').value = "";
}

function borrarOperacionF5(){
    document.getElementById('form:itNumeralDesOp').value = "";
    document.getElementById('form:itValorTotalUSDDesOp').value = "";
}

function validarValorMonetario(componente){
    var longitud=componente.value.toString().length;
    var posicionPunto=componente.value.toString().indexOf(".",0);

    if((event.keyCode > 47 && event.keyCode < 58 )|| (event.keyCode > 95 && event.keyCode < 106 )||  event.keyCode==110 || event.keyCode==190 ){
    	
    	if(longitud > 5 && posicionPunto < 0){
       	 this.event.returnValue=false;
       	componente.value=componente.value.toString().substr(0, componente.value.toString().length-1);
       		
       }	
       	
       }else{
       	
           this.event.returnValue=true;
       }
    	
    	if(posicionPunto!=-1 && longitud-posicionPunto>=8 ){
            this.event.returnValue=false;
            componente.value=componente.value.toString().substr(0, componente.value.toString().length-1);
        }else {    	
            this.event.returnValue=true;
        }
    	if(longitud > 5 && posicionPunto > 6){
   			this.event.returnValue=false;
   			componente.value=componente.value.toString().substr(0,5);
   		}else{
    
    
    	this.event.returnValue=true;
   		}
    	
    	this.event.returnValue=true;
    //alert(componente.value)
}

function deshabilitarBotonAdicionarClasificacion(){
    document.getElementById('form:cbAdicionarClasificacion').disabled=true;
	setTimeout ("habilitarBotonAdicionar()", 4000); 
}

function habilitarBotonAdicionar(){
	    document.getElementById('form:cbAdicionarClasificacion').disabled=false;
	
}





