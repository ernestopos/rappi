/* 
 * Funciones y Scripts comunes en JS
 */
PrimeFaces.locales['es'] = {
    closeText: 'Cerrar',
    prevText: 'Anterior',
    nextText: 'Siguiente',
    monthNames: ['Enero','Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
    dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
    dayNamesShort: ['Dom','Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
    dayNamesMin: ['D','L','M','X','J','V','S'],
    weekHeader: 'Semana',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: '',
    timeOnlyTitle: 'Sólo hora',
    timeText: 'Tiempo',
    hourText: 'Hora',
    minuteText: 'Minuto',
    secondText: 'Segundo',
    currentText: 'Fecha actual',
    ampm: false,
    month: 'Mes',
    week: 'Semana',
    day: 'Día',
    allDayText : 'Todo el día'
};

function titlesDetalleFuente() {

    $('.detalleFuenteDIV .ui-datatable.ui-widget.ui-datatable-resizable .ui-datatable-data.ui-widget-content tr').each (function(i,e){
        $('.ui-editable-column-options .ui-row-editor span',this).each (function(i2,e2){
            if(i2 == 0){
                $(this).attr('title','Editar');
            }
            else if(i2 == 1){
                $(this).attr('title','Guardar');
            }
            else{
                $(this).attr('title','Cancelar');
            }
        });
    });
}

function titlesDetalleDestino() {

    $('.detalleDestinoDIV .ui-datatable.ui-widget.ui-datatable-resizable .ui-datatable-data.ui-widget-content tr').each (function(i,e){
        $('.ui-editable-column-options .ui-row-editor span',this).each (function(i2,e2){
            if(i2 == 0){
                $(this).attr('title','Editar');
            }
            else if(i2 == 1){
                $(this).attr('title','Guardar');
            }
            else{
                $(this).attr('title','Cancelar');
            }
        });
    });
}


function titlesDetalleMapeo() {

    $('.detalleMapeoDIV .ui-datatable.ui-widget.ui-datatable-resizable .ui-datatable-data.ui-widget-content tr').each (function(i,e){
        $('.ui-editable-column-options .ui-commandlink .ui-row-editor span',this).each (function(i2,e2){
            if(i2 == 0){
                $(this).attr('title','Editar');
            }
            else if(i2 == 1){
                $(this).attr('title','Guardar');
            }
            else{
                $(this).attr('title','Cancelar');
            }
        });
    });
}






if ($.browser.msie && $.browser.version < 7 ) {
    location.href = "/SACDAV-war/ie6.html";
}


function handleRequestParam(args,id,dl) {  
    if(args.validationFailed) {  
        $('#'+id).effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 
        dl.hide();
    }  
} 

function handleRequestParam2(args,id) {  
    if(args.validationFailed) {  
        $('#'+id).effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } 
    if($("#"+msgDialog.id+" .ui-messages-error-summary").html()){
        msgDialog.show();
    }
} 

function retornaError(){
    if($("#"+msgDialog.id+" .ui-messages-error-summary").html()){
        msgDialog.show();
    }
}

function handleRequestAddParam(args,id,dialog,dialog2) {  
    if(args.validationFailed || args.error) {  
        $('#'+id).effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else{
        if($("#"+msgDialog.id+" .ui-messages-error-summary").html()){
            msgDialog.show();
        }else{
            msgDialog.show();
            dialog.hide();
            dialog2.hide();
        }
    }
     
} 

function handleAddRequest(args) {  
    if(args.validationFailed) {  
        jQuery('#dialogNew').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 
        msgDialog.show();
        dlgN.hide();
    }  
} 
function handleEditRequestDialog(args) {  
    if(args.validationFailed || args.error) {
        jQuery('#dialogView').effect("shake", {
            times:3
        }, 100);
        msgDialog.show();
        dlgV.hide();               
    } else {
    	alert('Else');
    	msgDialog.show();
    }  
} 

function handleRemRequest(args) {  

    msgDialog.show();
    dlgT.hide();
 
} 

function handleAddRequestmapeo(args) {  
    if(args.validationFailed || args.error) {  
        jQuery('#dialogNew').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 

        jQuery(".cancelarmapeo").click();
        setTimeout(function(){
            $(".ui-messages.ui-widget").html('<div class="ui-messages-info ui-corner-all"><span class="ui-messages-info-icon"></span><ul><li><span class="ui-messages-info-summary">El Mapeo se realizo exitosamente</span></li></ul></div>');
            msgDialog.show();
        },100);
           
               
    }  
} 



function handleAddRequest2(args) {  
    if(args.validationFailed) {  
        jQuery('#dialogNew').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 
          
        dlgN.hide();
    }  
} 
function handleAddRequest3(args) {  
    if(args.validationFailed || args.error) {  
        jQuery('#dialogNewA').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 
        msgDialog.show();
        dlgNA.hide();
    }  
} 
function handleEditRequest(args) {  
    if(args.validationFailed || args.error) {  
        jQuery('#dialogEdit').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 
        msgDialog.show();
        dlgE.hide();
    }  
} 




function handleEditTabRequest(args) {  
    if(args.validationFailed || args.error) {  
        msgDialog.show();
    } else { 
        msgDialog.show();
    }  
} 
function handleDeleteRequest(args) {  
    if(args.validationFailed || args.error) {  
        jQuery('#dialogDelete').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 
        msgDialog.show();
        dlgD.hide();
    }  
}
function handleDeleteDestinoRequest(args) {  
    if(args.validationFailed || args.error) {  
        jQuery('#dialogTrash').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 
        msgDialog.show();
        dlgD.hide();
    }  
} 
function handleDeleteDetalleRequest(args) {  
    if(args.validationFailed || args.error) {  
        jQuery('#dialogTrashDe').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 
        msgDialog.show();
          
    }  
} 



function handleSearchRequest(args) {  
    if(args.validationFailed || args.error) {  
        jQuery('#dialogSearch').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else {
        dlgS.hide();
        clis.filter()
    }  
}

function handleLoginRequest(args) {  
    if(args.validationFailed) {  
        jQuery('#msgDial').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 
        msgDialog.show();
    }  
}

function selectText(th) {
    if (document.selection) {
        var range = document.body.createTextRange();
        range.moveToElementText(th);
        range.select();
    } else if (window.getSelection) {
        var range = document.createRange();
        range.selectNode(th);
        window.getSelection().addRange(range);
    }
}


function handleRequest(args) {  
    if(args.validationFailed) {  
        jQuery('#dialogNew').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 
        msgDialog.show();
     
    }     
}

function handleRequestadd(args) {  
    if(args.validationFailed || args.error) {  
        jQuery('#dialogNew').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 
    	dlgV.show();
    }     
}

function handlefuntionaddRequest(args) {  

    if(args.validationFailed || args.error) {  
        jQuery('#dialogNew').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
        dlgF.hide();
    } else { 
        
        dlgF.hide();
    } 
}

function handlefuntionmodRequest(args) {  

    if(args.validationFailed || args.error) {  
        jQuery('#dialogNew').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
        dlgFE.hide();
    } else { 
        
        dlgFE.hide();
    } 
}



function handleRequestaddmap(args) {  
    if(args.validationFailed || args.error) {  
        msgDialog.show();
    }     
}


function handleRequestadd2(args) {  
    if(args.validationFailed || args.error) {  
        jQuery('#dialogNew').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 
        msgDialog.show();
        dlgN.hide();
    }     
}


function handleclikeditRequest(args) {  
    if(args.validationFailed || args.error) {  
        jQuery("[id='tabView:mapeoTablaMod:"+args.id+":roweditmapeo']").find('span.ui-icon-pencil').each(function(){
            jQuery(this).click()
        });
          
        msgDialog.show();
    } else { 
        msgDialog.show();
    }  
} 

function handleclikeditnewRequest(args) {  
    if(args.validationFailed || args.error) {  
        jQuery("[id='tabView:detalleFuenteTablaE:"+args.id+":rownewfuente']").find('span.ui-icon-pencil').each(function(){
            jQuery(this).click()
        });
          
        msgDialog.show();
    } else { 
        msgDialog.show();
    }  
} 

function handleclikaddModRequest2(args) {  
    if(args.validationFailed || args.error) {  
                      
        jQuery("[id='tabView:editForm:detalleFuenteTablaMod:"+args.id+":roweditfuente']").find('span.ui-icon-pencil').each(function(){
            jQuery(this).click()
        });
         
        msgDialog.show();
    } else { 
        msgDialog.show();
         
    }  
} 


function handleclikaddRequest(args) {  
    if(args.validationFailed || args.error) {  
        jQuery("[id='tabView:detalleFuenteTablaE:"+args.id+":rownewfuente']").find('span.ui-icon-pencil').each(function(){
            jQuery(this).click()
        });
        msgDialog.show();
    } else { 
        
        jQuery("[id='tabView:detalleFuenteTablaE']").find('span.ui-paginator-last.ui-state-default.ui-corner-all').each(function(){
            jQuery(this).click()
        });
        
        setTimeout(function(){
            jQuery('.ui-datatable-data tr').last().find('span.ui-icon-pencil').each(function(){
                jQuery(this).click()
            });
        },600);
        
   
        
    }  
} 

function handleclikaddModRequest(args) {  
   
    if(args.validationFailed || args.error) {  
        jQuery("[id='tabView:editForm:tablaDetalleFuente']").find('span.ui-paginator-last.ui-state-default.ui-corner-all').each(function(){
            jQuery(this).click()
        });
          
        setTimeout(function(){
            jQuery("[id='tabView:editForm:detalleFuenteTablaMod:"+args.id+":roweditfuente']").find('span.ui-icon-pencil').each(function(){
                jQuery(this).click()
            
            }); 
            $(".ui-messages.ui-widget").html('<div class="ui-messages-error ui-corner-all"><span class="ui-messages-error-icon"></span><ul><li><span class="ui-messages-error-summary">Se debe Validar el Campo Detalle Fuente</span></li></ul></div>');
            msgDialog.show();
        },600);
    
       
    } else { 
        
        jQuery("[id='tabView:editForm:tablaDetalleFuente']").find('span.ui-paginator-last.ui-state-default.ui-corner-all').each(function(){
            jQuery(this).click()
        });
        
        setTimeout(function(){
            jQuery('.ui-datatable-data tr').last().find('span.ui-icon-pencil').each(function(){
                jQuery(this).click()
            });
        },600);
      
        
    }  
} 


function handleDestinoRequest(args) {  
    if(args.validationFailed) {         
        msgDialog.show();
    }     
}

function emptyOnly(evt){
    // Backspace = 8, '0' = 48, '9' = 57, 0 = TAB y Flechas
    var nav4 = window.Event ? true : false;
    var ie = window.navigator.appName;
    if(ie == 'Microsoft Internet Explorer')
        nav4 = false;
    var key = nav4 ? evt.which : evt.keyCode;
    return ( key == 0);
}

function numbersOnly(evt){
    // Backspace = 8, '0' = 48, '9' = 57, 0 = TAB y Flechas
    var nav4 = window.Event ? true : false;
    var ie = window.navigator.appName;
    if(ie == 'Microsoft Internet Explorer')
        nav4 = false;
    var key = nav4 ? evt.which : evt.keyCode;
    return ((key >= 48 && key <= 57) || key==8 || key==9 || key == 0);
}



function numbersOrEnter(evt,id){
    // Backspace = 8, '0' = 48, '9' = 57, 0 = TAB y Flechas
    var nav4 = window.Event ? true : false;
    var ie = window.navigator.appName;
    if(ie == 'Microsoft Internet Explorer')
        nav4 = false;
    var key = nav4 ? evt.which : evt.keyCode;
    if(key == 13){
        $(id).click();
        return false
    }
    else{
        return ((key >= 48 && key <= 57) || key==8 || key==9 || key == 0);
    }
    
}

function handleAddDetalleDestino(args,id){
    if(args.validationFailed || args.error) { 
        jQuery("[id='tabView:destinoForm:detalleDestinoTabla:"+args.id+":"+id+"']").find('span.ui-icon-pencil').each(function(){
            jQuery(this).click()
        });
        msgDialog.show();
    } else { 
        
        jQuery("[id='tabView:destinoForm:detalleDestinoTabla']").find('span.ui-paginator-last.ui-state-default.ui-corner-all').each(function(){
            jQuery(this).click()
        });
        
        setTimeout(function(){
            jQuery('.ui-datatable-data tr').last().find('span.ui-icon-pencil').each(function(){
                jQuery(this).click()
            });
        },600);
        
        
     
    }
}

function handleEditDetalleDestino(args,id){
    if(args.validationFailed || args.error) { 
        jQuery("[id='tabView:destinoForm:detalleDestinoTabla:"+args.id+":"+id+"']").find('span.ui-icon-pencil').each(function(){
            jQuery(this).click()
        });
        msgDialog.show();
    } else { 
        msgDialog.show();
    }
}
function handleAddEDetalleDestino(args,id){
    if(args.validationFailed || args.error) { 
        jQuery("[id='tabView:editForm:detalleDestinoTablaMod:"+args.id+":"+id+"']").find('span.ui-icon-pencil').each(function(){
            jQuery(this).click()
        });
        msgDialog.show();
    } else { 
        
        jQuery("[id='tabView:editForm:tablaDetalleDestino']").find('span.ui-icon.ui-icon-seek-end').each(function(){
            jQuery(this).click()
        });
        
        setTimeout(function(){
            jQuery('.ui-datatable-data tr').last().find('span.ui-icon-pencil').each(function(){
                jQuery(this).click()
            });
        },600);
        
        
     
    }
}

function handleEditEDetalleDestino(args,id){
    if(args.validationFailed || args.error) { 
        jQuery("[id='tabView:editForm:detalleDestinoTablaMod:"+args.id+":"+id+"']").find('span.ui-icon-pencil').each(function(){
            jQuery(this).click()
        });
        msgDialog.show();
    } else { 
        msgDialog.show();
    }
}
function handleAddRequest4(args) {  
    if(args.validationFailed || args.error) {  
        jQuery('#dialogNewPrecedenciaPadre').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 
        msgDialog.show();
        dlgPPP.hide();
    }  
} 

function handleAddRequest5(args) {  
    if(args.validationFailed || args.error) {  
        jQuery('#dialogNewPrecedenciaHijas').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 
        msgDialog.show();
        dlgPPH.hide();
    }  
} 


function handleEditConPrcPadreRequest(args) {  
    if(args.validationFailed || args.error) {  
        jQuery('#dialogEditPP').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 
        msgDialog.show();
        dlgEP.hide();
    }  
} 






function handleEditConPrcHijaRequest(args) {  
    if(args.validationFailed || args.error) {  
        jQuery('#dialogEditPH').effect("shake", {
            times:3
        }, 100); 
        msgDialog.show();
    } else { 
        msgDialog.show();
        dlgEH.hide();
    }  
} 

function alphaNumericOnly(id, valor){
    var value = valor.replace(/[^-a-zA-Z0-9]/g, "");
    $("#"+id).val(value);
}

function alphaNumericWithoutSpacesOnly(id, valor){
    var value = valor.replace(/[^-a-zA-Z-.0-9ñÑ_]/g, "");
    $("#"+id).val(value);
}




/***SCRIPTS**/
//function resetDataTableMultiple(idDT){
//    $("#"+idDT+" table thead th").each (function(i,e){
//        if(i > 0){
//            $(this).attr('class','ui-state-default ui-sortable-column ui-filter-column ui-resizable-column ui-dynamic-column');
//            $('span',this).each (function(i2,e2){
//                if(i2==1){//posicion 1 es el segundo span
//                    $(this).attr('class','ui-sortable-column-icon ui-icon ui-icon-carat-2-n-s');
//                }
//            });
//        }
//    });
//}

function resetDataTable(idDT){
    $("#"+idDT+" table thead th").each (function(i,e){
        if(i > 0){
            $(this).attr('class','ui-state-default ui-sortable-column ui-filter-column ui-dynamic-column');
            $('span',this).each (function(i2,e2){
                if(i2==0){//primer elemento span a cambiar
                    $(this).attr('class','ui-sortable-column-icon ui-icon ui-icon-carat-2-n-s');
                }
            });
        }
    });
}

function resetDataTableResize(idDT){
    $("#"+idDT+" table thead th").each (function(i,e){
        if(i > 0){
            $(this).attr('class','ui-state-default ui-sortable-column ui-filter-column ui-resizable-column ui-dynamic-column');
            $('span',this).each (function(i2,e2){
                if(i2==1){
                    $(this).attr('class','ui-sortable-column-icon ui-icon ui-icon-carat-2-n-s');
                }
            });
        }
    });
}

function dinamiColumnDatable(idDT, idDTparam, hidden){//(id Datatable)
        
    $("#"+idDT+"_paginator_top,#"+idDT+"_paginator_bottom").live({
        mouseenter:function(){
            ejecutar(idDT, idDTparam,hidden);
            $("."+hidden+"Button").click();
        },
        mouseleave:function(){
            $("#"+hidden+"Input").val("");
            $("."+hidden+"Button").click();
        }
    });

    $("#"+idDT+" table thead .ui-state-default.ui-sortable-column.ui-filter-column").live({
        mouseenter:function(){
            $("#"+hidden+"Input").val("");
            $("."+hidden+"Button").click();
        }
    });

    $("#"+idDT+" table thead input").live({
        keyup: function(){
            ejecutar(idDT, idDTparam,hidden);
            $("."+hidden+"Button").click();
        },
        mouseleave: function(){
            if(this.value == ""){
                $("#"+hidden+"Input").val("");
            }
            $("."+hidden+"Button").click();
        }
    });
}

function dinamiColumnDatableMultiple(idDT, idDTparam, hidden){//(id General, id Param, hidden)
        
    $("#"+idDT+" .ui-paginator.ui-paginator-top.ui-widget-header, #"+idDT+" .ui-paginator.ui-paginator-bottom.ui-widget-header").live({
        mouseenter:function(){
            ejecutarMultiple(idDT, idDTparam,hidden);
            $("."+hidden+"Button").click();
        },
        mouseleave:function(){
            $("#"+hidden+"Input").val("");
            $("."+hidden+"Button").click();
        }
    });
    

    $("#"+idDT+" table thead .ui-state-default.ui-sortable-column.ui-filter-column").live({
        mouseenter:function(){
            $("#"+hidden+"Input").val("");
            $("."+hidden+"Button").click();
        }
    });

    $("#"+idDT+" table thead input").live({
        keyup: function(){
            ejecutarMultiple(idDT, idDTparam,hidden);
            $("."+hidden+"Button").click();
        },
        mouseleave: function(){
            if(this.value == ""){
                $("#"+hidden+"Input").val("");
            }
            $("."+hidden+"Button").click();
        }
    });
}

function ejecutarMultiple(idDT, idDTparam,hidden){//("id datatable" ,"div que guarda parametros adiciones")
    
    $("#"+idDT+" table thead tr th").each (function(i,th){
        sortId = $(th).attr("id");
        $("span",th).each(function(i2,span){
            if( $(span).attr("class") &&
                ($(span).attr("class").indexOf("ui-icon-triangle-1-s")>=0 || 
                    $(span).attr("class").indexOf("ui-icon-triangle-1-n")>=0)
                ){
                addSortMultiple(idDTparam, idDT, sortId,$(span).attr("class"),hidden);
            }
        });
    });
}
function addSortMultiple(idDTparam, idDT, sortId, clase,hidden){//agrega sort
    order = "";
    if(clase.indexOf("ui-icon-triangle-1-s")>=0){
        order = "DESCENDING";
    }else if(clase.indexOf("ui-icon-triangle-1-n")>=0){
        order = "ASCENDING"
    }
    
    $("#"+hidden+"Input").val("ok");
    $("#"+idDTparam).html("<input type='hidden' name='acordion:"+hidden+"_sortKey' value='"+sortId+"'/>"
        +"<input type='hidden' name='acordion:"+hidden+"_sorting' value='true'/>"
        +"<input type='hidden' name='acordion:"+hidden+"_skipChildren' value='true'/>"
        +"<input type='hidden' name='acordion:"+hidden+"_dynamic_column' value='true'/>"
        +"<input type='hidden' name='acordion:"+hidden+"_sortDir' value='"+order+"'/>");

}

function ejecutar(idDT, idDTparam,hidden){//("id datatable" ,"div que guarda parametros adiciones")
    
    $("#"+idDT+" table thead tr th").each (function(i,th){
        sortId = $(th).attr("id");
        $("span",th).each(function(i2,span){
            if( $(span).attr("class") &&
                ($(span).attr("class").indexOf("ui-icon-triangle-1-s")>=0 || 
                    $(span).attr("class").indexOf("ui-icon-triangle-1-n")>=0)
                ){
                addSort(idDTparam, idDT, sortId,$(span).attr("class"),hidden);
            }
        });
    });
}
function addSort(idDTparam, idDT, sortId, clase,hidden){//agrega sort
    order = "";
    if(clase.indexOf("ui-icon-triangle-1-s")>=0){
        order = "DESCENDING";
    }else if(clase.indexOf("ui-icon-triangle-1-n")>=0){
        order = "ASCENDING"
    }
    
    $("#"+hidden+"Input").val("ok");
    $("#"+idDTparam).html("<input type='hidden' name='"+idDT+"_sortKey' value='"+sortId+"'/>"
        +"<input type='hidden' name='"+idDT+"_sorting' value='true'/>"
        +"<input type='hidden' name='"+idDT+"_skipChildren' value='true'/>"
        +"<input type='hidden' name='"+idDT+"_dynamic_column' value='true'/>"
        +"<input type='hidden' name='"+idDT+"_sortDir' value='"+order+"'/>");

}

function limpiaMessage(idDT){
    $("#"+idDT+" .ui-widget-content.ui-datatable-empty-message").hide();
}

function eliminaFiltros(idDT){
    $("#"+idDT+" table thead tr th.ui-state-default.ui-sortable-column.ui-filter-column.ui-dynamic-column").each (function(i,th){
        if($("input",th).css("display") == 'none'){
            $('span',th).each (function(i2,span){
                if($(span).attr("class") && $(span).attr("class").indexOf("ui-sortable-column-icon")>=0){
                    $(span).remove();
                }
            });
        }
    });
}

function center(id){
    $("#"+id).css("top","50%");
    $("#"+id).css("left","50%");
    w = $("#"+id).css("width");
    h = $("#"+id).css("height");
    w = w.substring(w, w.length-2);
    h = h.substring(h, h.length-2);
    marginw = "-"+Math.ceil(w/2)+"px";
    marginh = "-"+Math.ceil(h/2)+"px";
    $("#"+id).css("margin-left",marginw);
    $("#"+id).css("margin-top",marginh);
}