package com.project.rappi.cubesumation.services;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.inject.Default;
import javax.inject.Inject;

import com.project.rappi.cubesumation.common.enums.BussinesLogicErrorEnum;
import com.project.rappi.cubesumation.common.enums.TransactionType;
import com.project.rappi.cubesumation.common.enums.ValidationDataEnum;
import com.project.rappi.cubesumation.common.exceptions.BussinesLogicExceptions;
import com.project.rappi.cubesumation.common.logs.ILog;
import com.project.rappi.cubesumation.common.logs.ILogDrivenImpl;
import com.project.rappi.cubesumation.common.mapping.mediator.IDozerMapping;
import com.project.rappi.cubesumation.common.mapping.mediator.IDozerMappingDriven;
import com.project.rappi.cubesumation.common.messages.propertiesmediator.IPropertieDriven;
import com.project.rappi.cubesumation.common.messages.propertiesmediator.Iproperties;

@Default
public class TransversalUtilityService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Iproperties
	@Inject
	public IPropertieDriven iPropertieDriven;

	@IDozerMapping
	@Inject
	public IDozerMappingDriven idozerMappingDriven;

	@ILog
	@Inject
	public ILogDrivenImpl iLogDrivenImpl;

	private BussinesLogicErrorEnum retorno;
	
	public void validarSentencias(List<String> executeLine) throws BussinesLogicExceptions {
		retorno = null;
		executeLine.forEach(s -> {
			if (!s.trim().toUpperCase().startsWith(TransactionType.UPDATE.name()) && !s.trim().toUpperCase().startsWith(TransactionType.QUERY.name())) {
				retorno = BussinesLogicErrorEnum.LINEA_COMANDO_INVALIDO;				
			}
			String[] sentencias = s.split(" ");
			
			if (s.trim().toUpperCase().startsWith(TransactionType.UPDATE.name())){
				if(sentencias.length!=ValidationDataEnum.UPDATE.getValor()){
					retorno = BussinesLogicErrorEnum.LINEA_COMANDO_INVALIDO;					
				}
			}			
			if (s.trim().toUpperCase().startsWith(TransactionType.QUERY.name())){
				if(sentencias.length!=ValidationDataEnum.QUERY.getValor()){
					retorno = BussinesLogicErrorEnum.LINEA_COMANDO_INVALIDO;					
				}
			}
		});
		if (retorno != null)
			throw new BussinesLogicExceptions(retorno);
	}
}