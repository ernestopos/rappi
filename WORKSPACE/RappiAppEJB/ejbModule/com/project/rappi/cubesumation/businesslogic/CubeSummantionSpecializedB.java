package com.project.rappi.cubesumation.businesslogic;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.inject.Inject;

import com.project.rappi.cubesumation.businesslogic.persistmanaged.PersistDriverManagedSB;
import com.project.rappi.cubesumation.common.dto.ExecuteQueryResultDTO;
import com.project.rappi.cubesumation.common.dto.MatrixVDTO;
import com.project.rappi.cubesumation.common.dto.ParametrosSQLDTO;
import com.project.rappi.cubesumation.common.enums.TransactionType;
import com.project.rappi.cubesumation.services.TransversalUtilityService;

public class CubeSummantionSpecializedB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EJB
	PersistDriverManagedSB<MatrixVDTO> persistDriverManagedSB;

	@Inject
	TransversalUtilityService transversalUtilityService;
	
	/***
	 * Permite persistir un objecto
	 * @param object
	 */
	public void persistObject(Object object) {
		try {
			persistDriverManagedSB.persistEntity(object);
		} catch (Exception e) {
			transversalUtilityService.iLogDrivenImpl.wlogger(Level.SEVERE, "CubeSummantionSpecializedB","persistCube", e.getMessage());
		}
	}
	
	/***
	 * Obtine un objecto
	 * @param cla
	 * @param id
	 * @return
	 */
	public Object getObject(Class<?> cla, BigInteger id) {
		Object object = null;
		try {
			object = persistDriverManagedSB.getObject(cla, id);
		} catch (Exception e) {
			transversalUtilityService.iLogDrivenImpl.wlogger(Level.SEVERE, "CubeSummantionSpecializedB", "getObject",e.getMessage());
		}
		return object;
	}
	
	/***
	 * Elimina los valores del cubo
	 * @param id
	 */
	public void deleteCubeValues(BigInteger id) {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM MatrixV WHERE matrixencid.id = :idMatrix");
			List<ParametrosSQLDTO> parametros = new ArrayList<>();
			parametros.add(new ParametrosSQLDTO("idMatrix", id));
			persistDriverManagedSB.sqlQueryExecute(TransactionType.UPDATE, sql.toString(), parametros);
		} catch (Exception e) {
			transversalUtilityService.iLogDrivenImpl.wlogger(Level.SEVERE, "CubeSummantionSpecializedB","deleteCubeValues", e.getMessage());
		}
	}
	
	/***
	 * Ejecuta la instrucción UPPDATE
	 * @param x
	 * @param y
	 * @param z
	 * @param valor
	 * @param cubeheader
	 */
	public void updateCubeValues(BigInteger x, BigInteger y, BigInteger z, BigInteger valor, BigInteger cubeheader) {
		try {
			StringBuilder sql = new StringBuilder();
			List<ParametrosSQLDTO> parametros = new ArrayList<>();
			sql.append(" UPDATE MatrixV matr SET matr.numvalue = :numValSetIntro ");
			sql.append(" WHERE matr.numx = :numxIntro AND matr.numy= :numyIntro AND matr.numz= :numzIntro AND matr.matrixencid.id= :matrixIdIntro");
			parametros.add(new ParametrosSQLDTO("numValSetIntro", valor));
			parametros.add(new ParametrosSQLDTO("numxIntro", x));
			parametros.add(new ParametrosSQLDTO("numyIntro", y));
			parametros.add(new ParametrosSQLDTO("numzIntro", z));
			parametros.add(new ParametrosSQLDTO("matrixIdIntro", cubeheader));
			persistDriverManagedSB.sqlQueryExecute(TransactionType.UPDATE, sql.toString(), parametros);
			persistDriverManagedSB.flush();

		} catch (Exception e) {
			transversalUtilityService.iLogDrivenImpl.wlogger(Level.SEVERE, "CubeSummantionSpecializedB",
					"updateCubeValues", e.getMessage());
		}
	}
	
	/***
	 * Ejecuta la instrucción QUERY
	 * @param x
	 * @param y
	 * @param z
	 * @param x2
	 * @param y2
	 * @param z2
	 * @param cubeheader
	 * @return
	 */
	public ExecuteQueryResultDTO<MatrixVDTO> queryCubeValues(BigInteger x, BigInteger y, BigInteger z, BigInteger x2,
			BigInteger y2, BigInteger z2, BigInteger cubeheader) {
		ExecuteQueryResultDTO<MatrixVDTO> retorno = new ExecuteQueryResultDTO<>();
		try {
			StringBuilder sql = new StringBuilder();
			List<ParametrosSQLDTO> parametros = new ArrayList<>();
			sql.append(" SELECT new com.project.rappi.cubesumation.common.dto.MatrixVDTO(sum(valo.numvalue)) ");
			sql.append(" FROM MatrixV valo ");
			sql.append(" INNER JOIN valo.matrixencid encma ");
			sql.append(" WHERE     ( valo.numx >= :numxIntro AND valo.numx <=:numxIntro2 ) ");
			sql.append("       AND ( valo.numy >= :numyIntro AND valo.numy <=:numyIntro2 ) ");
			sql.append("       AND ( valo.numz >= :numzIntro AND valo.numz <=:numzIntro2 ) ");
			sql.append("       AND ( encma.id = :matrixIdIntro ) ");
			parametros = new ArrayList<>();
			parametros.add(new ParametrosSQLDTO("numxIntro", x));
			parametros.add(new ParametrosSQLDTO("numyIntro", y));
			parametros.add(new ParametrosSQLDTO("numzIntro", z));
			parametros.add(new ParametrosSQLDTO("numxIntro2", x2));
			parametros.add(new ParametrosSQLDTO("numyIntro2", y2));
			parametros.add(new ParametrosSQLDTO("numzIntro2", z2));
			parametros.add(new ParametrosSQLDTO("matrixIdIntro", cubeheader));
			retorno = persistDriverManagedSB.sqlQueryExecute(TransactionType.SELECT, sql.toString(), parametros);
		} catch (Exception e) {
			transversalUtilityService.iLogDrivenImpl.wlogger(Level.SEVERE, "CubeSummantionSpecializedB","queryCubeValues", e.getMessage());
		}
		return retorno;
	}
}
