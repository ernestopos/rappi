package com.project.rappi.cubesumation.businesslogic.persistmanaged;

import java.math.BigInteger;
import java.util.List;

import com.project.rappi.cubesumation.common.dto.ExecuteQueryResultDTO;
import com.project.rappi.cubesumation.common.dto.ParametrosSQLDTO;
import com.project.rappi.cubesumation.common.enums.TransactionType;

public interface PersistDriverManagedSBLocal<T> {
	
	/***
	 * Gestiona las transacciones persistentes con el manejador de base de datos.
	 * @param transactionType
	 * @param sql
	 * @param parametros
	 * @return
	 */
	public ExecuteQueryResultDTO<T> sqlQueryExecute(TransactionType transactionType, String sql,
			List<ParametrosSQLDTO> parametros);
	
	/***
	 * Gestiona la persistencia de una entity bean
	 * @param o
	 * @return
	 */
	public Object persistEntity(Object o);
	
	/***
	 * Sincroniza la base de datos
	 */
	public void flush();
	
	public Object getObject(Class<?> clase, BigInteger id);
	
}
