package com.project.rappi.cubesumation.businesslogic.persistmanaged;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.project.rappi.cubesumation.common.dto.ExecuteQueryResultDTO;
import com.project.rappi.cubesumation.common.dto.ParametrosSQLDTO;
import com.project.rappi.cubesumation.common.enums.TransactionType;
import com.project.rappi.cubesumation.services.TransversalUtilityService;

import javax.ejb.TransactionAttribute;

@Stateless
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class PersistDriverManagedSB<T> implements Serializable, PersistDriverManagedSBLocal<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	TransversalUtilityService transversalUtilityService;

	@PersistenceContext(unitName = "RAPPIAPP_JDBC")
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public ExecuteQueryResultDTO<T> sqlQueryExecute(TransactionType transactionType, String sql,
			List<ParametrosSQLDTO> parametros) {

		ExecuteQueryResultDTO<T> retur = new ExecuteQueryResultDTO<>();

		TypedQuery<T> q = null;
		switch (transactionType) {
		case UPDATE:
		case DELETE:
		case INSERT:
		case SELECT:
			try {
				if (transactionType.compareTo(TransactionType.SELECT) == 0) {
					q = entityManager.createQuery(sql.toString(), (Class<T>) getClass());
				} else {
					q = (TypedQuery<T>) entityManager.createQuery(sql.toString());
				}
				if (parametros != null && !parametros.isEmpty()) {
					for (ParametrosSQLDTO parametro : parametros) {
						q.setParameter(parametro.getKey(), parametro.getValue());
					}
				}
				if (transactionType.compareTo(TransactionType.SELECT) == 0) {
					retur.setResultMessage(new StringBuilder("RESULT DML: ").append(BigInteger.ONE).toString());
					retur.setResultList(q.getResultList());
				} else {
					retur.setResultMessage(new StringBuilder("RESULT DML: ").append(q.executeUpdate()).toString());
				}
			} catch (Exception e) {
				transversalUtilityService.iLogDrivenImpl.wlogger(Level.SEVERE, "PersistDriverManagedSB",
						"sqlQueryExecute", e.getMessage());
			}
		default:
			break;
		}

		return retur;
	}

	@Override
	public Object persistEntity(Object o) {
		try {
			entityManager.persist(o);
		} catch (Exception e) {
			transversalUtilityService.iLogDrivenImpl.wlogger(Level.SEVERE, "PersistDriverManagedSB", "persistEntity",
					e.getMessage());
		}
		return o;
	}

	@Override
	public void flush() {
		entityManager.flush();
	}
	
	@Override
	public Object getObject(Class<?> clase, BigInteger id) {
		return entityManager.getReference(clase, id);
	}
}