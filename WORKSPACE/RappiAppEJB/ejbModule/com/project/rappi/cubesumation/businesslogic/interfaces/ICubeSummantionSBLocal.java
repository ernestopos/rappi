package com.project.rappi.cubesumation.businesslogic.interfaces;

import javax.ejb.Local;

import com.project.rappi.cubesumation.common.dto.*;
import com.project.rappi.cubesumation.common.exceptions.BussinesLogicExceptions;

@Local
public interface ICubeSummantionSBLocal {
	public ExecuteProcessResponseDTO executeCubeSummation(ExecuteProcessDTO execute)throws BussinesLogicExceptions;
}
