package com.project.rappi.cubesumation.modelo;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "MATRIXV")
@SequenceGenerator(name = "SEQ_MATRIXV", sequenceName = "SEQ_MATRIXV_ID", allocationSize = 1)
public class MatrixV implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MATRIXV")
	private BigInteger id;

	@ManyToOne
	@JoinColumn(name = "MATRIX_ENC_ID", referencedColumnName = "ID")
	private Matrix matrixencid;

	@Column(name = "NUM_X", nullable = false)
	private BigInteger numx;

	@Column(name = "NUM_Y", nullable = false)
	private BigInteger numy;

	@Column(name = "NUM_Z", nullable = false)
	private BigInteger numz;

	@Column(name = "NUM_VALUE", nullable = false)
	private BigInteger numvalue;

	public MatrixV() {
	}
	
	public MatrixV(BigInteger numx, BigInteger numy, BigInteger numz, BigInteger numvalue) {
		this.numx = numx;
		this.numy = numy;
		this.numz = numz;
		this.numvalue = numvalue;
	}

	public BigInteger getId() {
		return id;
	}	

	public void setId(BigInteger id) {
		this.id = id;
	}

	public BigInteger getNumx() {
		return numx;
	}

	public void setNumx(BigInteger numx) {
		this.numx = numx;
	}

	public BigInteger getNumy() {
		return numy;
	}

	public void setNumy(BigInteger numy) {
		this.numy = numy;
	}

	public BigInteger getNumz() {
		return numz;
	}

	public void setNumz(BigInteger numz) {
		this.numz = numz;
	}

	public BigInteger getNumvalue() {
		return numvalue;
	}

	public void setNumvalue(BigInteger numvalue) {
		this.numvalue = numvalue;
	}

	public Matrix getMatrixencid() {
		return matrixencid;
	}

	public void setMatrixencid(Matrix matrixencid) {
		this.matrixencid = matrixencid;
	}	
}
