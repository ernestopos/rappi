package com.project.rappi.cubesumation.modelo;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "MATRIX")
@SequenceGenerator(name = "SEQ_MATRIX", sequenceName = "SEQ_MATRIX_ID", allocationSize = 1)
public class Matrix implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_MATRIX")
	private BigInteger id;

	@Column(name = "STR_NOMBRE", nullable = false)
	private String strnombre;

	@Column(name = "DTM_FECHA", nullable = false)
	private Timestamp dtmfecha;

	@Column(name = "NUM_SIZE", nullable = false)
	private BigInteger numsize;

	public Matrix(String strnombre, Timestamp dtmfecha, BigInteger numsize) {
		this.dtmfecha = dtmfecha;
		this.numsize = numsize;
	}

	public Matrix() {

	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}	

	public String getStrnombre() {
		return strnombre;
	}

	public void setStrnombre(String strnombre) {
		this.strnombre = strnombre;
	}

	public Timestamp getDtmfecha() {
		return dtmfecha;
	}

	public void setDtmfecha(Timestamp dtmfecha) {
		this.dtmfecha = dtmfecha;
	}

	public BigInteger getNumsize() {
		return numsize;
	}

	public void setNumsize(BigInteger numsize) {
		this.numsize = numsize;
	}
}
