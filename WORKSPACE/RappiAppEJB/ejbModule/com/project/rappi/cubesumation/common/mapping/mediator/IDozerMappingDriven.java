package com.project.rappi.cubesumation.common.mapping.mediator;

public interface IDozerMappingDriven {	
	public <T> T copydata(Object source, Class<T> target, String mappingId);
	public void copydata(Object source, Object target, String mappingId);
}