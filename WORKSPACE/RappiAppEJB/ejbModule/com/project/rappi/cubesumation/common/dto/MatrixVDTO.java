package com.project.rappi.cubesumation.common.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class MatrixVDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;
	private MatrixDTO matrixencid;
	private BigInteger numx;
	private BigInteger numy;
	private BigInteger numz;
	private BigInteger numvalue;
	private BigInteger sumquery;

	public MatrixVDTO(BigInteger sumquery) {
		this.sumquery = sumquery;
	}

	public MatrixVDTO(BigInteger id, BigInteger idMatrizEnc, BigInteger numx, BigInteger numy, BigInteger numz,
			BigInteger numvalue) {
		this.id = id;
		this.matrixencid = new MatrixDTO(idMatrizEnc);
		this.numx = numx;
		this.numy = numy;
		this.numz = numz;
		this.numvalue = numvalue;
	}

	public MatrixVDTO() {

	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}	

	public MatrixDTO getMatrixencid() {
		return matrixencid;
	}

	public void setMatrixencid(MatrixDTO matrixencid) {
		this.matrixencid = matrixencid;
	}

	public BigInteger getNumx() {
		return numx;
	}

	public void setNumx(BigInteger numx) {
		this.numx = numx;
	}

	public BigInteger getNumy() {
		return numy;
	}

	public void setNumy(BigInteger numy) {
		this.numy = numy;
	}

	public BigInteger getNumz() {
		return numz;
	}

	public void setNumz(BigInteger numz) {
		this.numz = numz;
	}

	public BigInteger getNumvalue() {
		return numvalue;
	}

	public void setNumvalue(BigInteger numvalue) {
		this.numvalue = numvalue;
	}

	public BigInteger getSumquery() {
		return sumquery;
	}

	public void setSumquery(BigInteger sumquery) {
		this.sumquery = sumquery;
	}
}