package com.project.rappi.cubesumation.common.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

public class MatrixDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigInteger id;
	private String strnombre;
	private Timestamp dtmfecha;
	private BigInteger numsize;

	public MatrixDTO(BigInteger id) {
		this.id = id;
	}

	public MatrixDTO(String strnombre, BigInteger numsize, Timestamp dtmfecha) {
		this.numsize = numsize;
		this.dtmfecha = dtmfecha;
	}

	public MatrixDTO(BigInteger id, Timestamp dtmfecha, BigInteger numsize) {
		this.id = id;
		this.dtmfecha = dtmfecha;
		this.numsize = numsize;
	}

	public MatrixDTO() {
		super();
	}

	public BigInteger getId() {
		return id;
	}

	public void setId(BigInteger id) {
		this.id = id;
	}

	public String getStrnombre() {
		return strnombre;
	}

	public void setStrnombre(String strnombre) {
		this.strnombre = strnombre;
	}

	public Timestamp getDtmfecha() {
		return dtmfecha;
	}

	public void setDtmfecha(Timestamp dtmfecha) {
		this.dtmfecha = dtmfecha;
	}

	public BigInteger getNumsize() {
		return numsize;
	}

	public void setNumsize(BigInteger numsize) {
		this.numsize = numsize;
	}
}