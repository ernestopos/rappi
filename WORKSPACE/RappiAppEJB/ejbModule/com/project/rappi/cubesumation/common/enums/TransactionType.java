package com.project.rappi.cubesumation.common.enums;

public enum TransactionType {
	SELECT,
	UPDATE,
	DELETE,
	INSERT,
	QUERY
}
