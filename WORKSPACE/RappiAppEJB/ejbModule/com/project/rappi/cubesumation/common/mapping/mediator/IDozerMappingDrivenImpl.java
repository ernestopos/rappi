package com.project.rappi.cubesumation.common.mapping.mediator;

import org.dozer.Mapper;

import com.project.rappi.cubesumation.common.mapping.DozerBeanMapperSingletonWrapper;

@IDozerMapping
public class IDozerMappingDrivenImpl implements IDozerMappingDriven {

	private final Mapper mapper;

	public IDozerMappingDrivenImpl() {
		mapper = DozerBeanMapperSingletonWrapper.getInstance();
	}

	@Override
	public <T> T copydata(Object source, Class<T> target, String mappingId) {
		return mapper.map(source, target, mappingId);
	}

	@Override
	public void copydata(Object source, Object target, String mappingId) {
		mapper.map(source, target, mappingId);
	}
}