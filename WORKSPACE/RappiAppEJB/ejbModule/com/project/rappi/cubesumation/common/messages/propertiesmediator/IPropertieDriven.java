package com.project.rappi.cubesumation.common.messages.propertiesmediator;

public interface IPropertieDriven {
	public String getKeyValue(String key);
}