package com.project.rappi.cubesumation.common.mapping;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.dozer.util.DozerConstants;

public class DozerBeanMapperSingletonWrapper {

	private static Mapper instance;

	public DozerBeanMapperSingletonWrapper() {}	
	public static synchronized Mapper getInstance() {
		try {
			if (instance == null) {
				List<String> mappingFiles = new ArrayList<>();
				mappingFiles.add(DozerConstants.DEFAULT_MAPPING_FILE);
				instance = new DozerBeanMapper(mappingFiles);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return instance;
	}

}
