package com.project.rappi.cubesumation.common.logs;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

@ILog
public class ILogDrivenImpl implements ILogDriven, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(ILogDrivenImpl.class.getPackage().getName());

	@Override
	public void wlogger(Level level, String classname, String sourceMethod, String message) {
		LOGGER.logp(level, classname, sourceMethod, message);
	}
}