package com.project.rappi.cubesumation.common.logs;

import java.util.logging.Level;

public interface ILogDriven {	
	public void wlogger(Level level, String classname, String sourceMethod, String message);
}