package com.project.rappi.cubesumation.common.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ExecuteQueryResultDTO<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<T> resultList = new ArrayList<>();
	private List<Object[]> resultListObjects = new ArrayList<>();
	private String resultMessage;

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public List<T> getResultList() {
		return resultList;
	}

	public void setResultList(List<T> resultList) {
		this.resultList = resultList;
	}

	public List<Object[]> getResultListObjects() {
		return resultListObjects;
	}

	public void setResultListObjects(List<Object[]> resultListObjects) {
		this.resultListObjects = resultListObjects;
	}	
}