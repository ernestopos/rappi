package com.project.rappi.cubesumation.common.enums;

public enum ConfigEnum {
	
	MESSAGE_PROPERTIES("messages.properties"),
	DOZER_MAPPING("dozerBeanMapping.xml"),
	PRINCIPAL_FORM(":principalForm");

	private String value;

	private ConfigEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}