package com.project.rappi.cubesumation.common.exceptions;

public class OperationExceptions extends Exception {
	private static final long serialVersionUID = 1L;

	public OperationExceptions(String message) {
		super(message);
	}
}