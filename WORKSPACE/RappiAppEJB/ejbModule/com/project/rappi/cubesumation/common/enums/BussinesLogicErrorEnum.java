package com.project.rappi.cubesumation.common.enums;

public enum BussinesLogicErrorEnum {
	
	TAMANIO_DEL_CUBO(101, "Se requiere el tama�o de la cubo"),
	VALOR_NUMERICO_INVALIDO(102, "El n�mero ingresado no es v�lido"),
	VALOR_MENOR_0(103, "El n�mero ingresado es menor que 0"),
	LINEA_COMANDO_INVALIDO(104, "La l�nea de comando es inv�lida"),
	LINEA_COMANDO_YA_EXISTE(105,"La l�nea de comando ingresada yaa existe"),
	NO_HAY_COMANDO_CARGADO( 106,"Por lo menos cargue un comando");

	private int code;
	private String message;

	private BussinesLogicErrorEnum(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}	
}