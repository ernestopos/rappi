package com.project.rappi.cubesumation.common.enums;

public enum ValidationDataEnum {
	
	UPDATE(5), 
	QUERY(7);

	private int valor;

	private ValidationDataEnum(int valor) {
		this.valor = valor;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}
}