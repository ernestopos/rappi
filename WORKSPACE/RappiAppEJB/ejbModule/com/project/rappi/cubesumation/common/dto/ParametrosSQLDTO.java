package com.project.rappi.cubesumation.common.dto;

import java.io.Serializable;

public class ParametrosSQLDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String key;
	private Object value;

	public ParametrosSQLDTO() {
		super();
	}

	public ParametrosSQLDTO(String key, Object value) {
		super();
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
}
