package com.project.rappi.cubesumation.common.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ExecuteProcessResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<String> response = new ArrayList<>();;

	public ExecuteProcessResponseDTO() {
		super();
	}

	public List<String> getResponse() {
		return response;
	}

	public void setResponse(List<String> response) {
		this.response = response;
	}
}
