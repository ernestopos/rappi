package com.project.rappi.cubesumation.common.mapping.impl;

import org.dozer.Mapper;

import com.project.rappi.cubesumation.common.mapping.DozerBeanMapperSingletonWrapper;



public class UtilidadesDataMapping {

	private static UtilidadesDataMapping instance;
	private final Mapper mapper;

	private UtilidadesDataMapping() {
		mapper = DozerBeanMapperSingletonWrapper.getInstance();
	}

	public static UtilidadesDataMapping getIntance() {
		if (instance == null) {
			instance = new UtilidadesDataMapping();
		}
		return instance;
	}

	public <T> T copydata(Object source, Class<T> target, String mappingId) {
		return mapper.map(source, target, mappingId);
	}

	public void copydata(Object source, Object target, String mappingId) {
		mapper.map(source, target, mappingId);
	}
}
