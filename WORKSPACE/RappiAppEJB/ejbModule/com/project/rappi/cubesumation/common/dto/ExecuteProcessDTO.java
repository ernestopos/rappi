package com.project.rappi.cubesumation.common.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ExecuteProcessDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private MatrixDTO matrixDTO;
	private List<String> executeLine = new ArrayList<>();;

	public ExecuteProcessDTO() {
		this.matrixDTO = new MatrixDTO();
	}

	public ExecuteProcessDTO(String nombre,BigInteger numsize, Timestamp dtmfecha){
		this.matrixDTO = new MatrixDTO(nombre,numsize, dtmfecha);
	}

	public MatrixDTO getMatrixDTO() {
		return matrixDTO;
	}

	public void setMatrixDTO(MatrixDTO matrixDTO) {
		this.matrixDTO = matrixDTO;
	}

	public List<String> getExecuteLine() {
		return executeLine;
	}

	public void setExecuteLine(List<String> executeLine) {
		this.executeLine = executeLine;
	}
}
