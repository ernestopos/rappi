package com.project.rappi.cubesumation.common.exceptions;

import com.project.rappi.cubesumation.common.enums.BussinesLogicErrorEnum;

public class BussinesLogicExceptions extends Exception {
	private static final long serialVersionUID = 1L;
	public BussinesLogicExceptions(BussinesLogicErrorEnum bussinesLogicErrorEnum) {
		super(new StringBuilder().append(bussinesLogicErrorEnum.getMessage()).toString());
	}
}