package com.project.rappi.cubesumation.common.messages;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Properties;

import javax.annotation.PostConstruct;

import com.project.rappi.cubesumation.common.enums.ConfigEnum;
import com.project.rappi.cubesumation.common.messages.propertiesmediator.IPropertieDriven;
import com.project.rappi.cubesumation.common.messages.propertiesmediator.Iproperties;

@Iproperties
public class IPropertiesImp implements IPropertieDriven, Serializable {

	private static final long serialVersionUID = 1L;

	Properties prop = new Properties();
	InputStream in = null;

	@PostConstruct
	public void ini() {
		try {
			InputStream in = getClass().getResourceAsStream(ConfigEnum.MESSAGE_PROPERTIES.getValue());
			prop.load(in);
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public String getKeyValue(String key) {
		return prop.getProperty(key);
	}
}