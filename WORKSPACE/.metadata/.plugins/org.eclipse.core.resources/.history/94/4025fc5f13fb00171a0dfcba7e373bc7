package com.project.rappi.cubesumation.modelo;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CLIENTE")
@SequenceGenerator(name = "SEQ_CLIENTE_ID", sequenceName = "SEQ_CLIENTE", allocationSize = 1)
public class Cliente implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "IDCLIENTE", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_CLIENTE_ID")
	private BigInteger idcliente;

	@Column(name = "STGNOMBRE", nullable = false)
	private String stgnombre;

	@Column(name = "STGAPELLIDO", nullable = false)
	private String stgapellido;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
	@JoinColumn(name = "IDTIPODOCUMENTO", referencedColumnName = "IDPARAMETRO")
	private Parametro idtipodocumento;

	@Column(name = "NUMNUMERODOCUMENTO", nullable = false)
	private BigInteger numnumerodocumento;

	@Column(name = "DTMFECHACREACION", nullable = false)
	private Timestamp fechacreacion;

	@Column(name = "NUMTELCONTACTO", nullable = false)
	private String telcontacto;

	@Column(name = "NUMACTIVO", nullable = false)
	private BigInteger numactivo;

	public Cliente() {
		super();
	}

	public Cliente(BigInteger idcliente, String stgnombre, String stgapellido, Parametro idtipodocumento,
			BigInteger numnumerodocumento, Timestamp fechacreacion, String telcontacto, BigInteger numactivo) {
		this.idcliente = idcliente;
		this.stgnombre = stgnombre;
		this.stgapellido = stgapellido;
		this.idtipodocumento = idtipodocumento;
		this.numnumerodocumento = numnumerodocumento;
		this.fechacreacion = fechacreacion;
		this.telcontacto = telcontacto;
		this.numactivo = numactivo;
	}

	public BigInteger getIdcliente() {
		return idcliente;
	}

	public String getStgnombre() {
		return stgnombre;
	}

	public void setStgnombre(String stgnombre) {
		this.stgnombre = stgnombre;
	}

	public String getStgapellido() {
		return stgapellido;
	}

	public void setStgapellido(String stgapellido) {
		this.stgapellido = stgapellido;
	}

	public Parametro getIdtipodocumento() {
		return idtipodocumento;
	}

	public void setIdtipodocumento(Parametro idtipodocumento) {
		this.idtipodocumento = idtipodocumento;
	}

	public BigInteger getNumnumerodocumento() {
		return numnumerodocumento;
	}

	public void setNumnumerodocumento(BigInteger numnumerodocumento) {
		this.numnumerodocumento = numnumerodocumento;
	}

	public Timestamp getFechacreacion() {
		return fechacreacion;
	}

	public void setFechacreacion(Timestamp fechacreacion) {
		this.fechacreacion = fechacreacion;
	}

	public String getTelcontacto() {
		return telcontacto;
	}

	public void setTelcontacto(String telcontacto) {
		this.telcontacto = telcontacto;
	}

	public BigInteger getNumactivo() {
		return numactivo;
	}

	public void setNumactivo(BigInteger numactivo) {
		this.numactivo = numactivo;
	}
}