package com.project.rappi.cubesumation.modelo;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "PRECIOVENTA")
@SequenceGenerator(name = "SEQ_PRECIOVENTA_ID", sequenceName = "SEQ_PRECIOVENTA", allocationSize = 1)
public class PrecioVenta implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "IDPRECIO", nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_PRECIOVENTA_ID")
	private BigInteger idprecio;

	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
	@JoinColumn(name = "IDARTICULO", referencedColumnName = "IDARTICULO")
	private Articulo idarticulo;

	@Column(name = "STGDESCRIPCION", nullable = false)
	private String stgdescripcion;

	@Column(name = "DTMFECHA", nullable = false)
	private Timestamp dtmfecha;

	@Column(name = "NUMPRECIO", nullable = false)
	private Double numprecio;

	@Column(name = "NUMACTIVO", nullable = false)
	private BigInteger numactivo;

	public PrecioVenta(BigInteger idprecio) {
		this.idprecio = idprecio;
	}

	public PrecioVenta() {
		super();
	}

	public PrecioVenta(BigInteger idprecio, Articulo idarticulo, String stgdescripcion, Timestamp dtmfecha,
			Double numprecio, BigInteger numactivo) {
		this.idprecio = idprecio;
		this.idarticulo = idarticulo;
		this.stgdescripcion = stgdescripcion;
		this.dtmfecha = dtmfecha;
		this.numprecio = numprecio;
		this.numactivo = numactivo;
	}

	public BigInteger getIdprecio() {
		return idprecio;
	}

	public void setIdprecio(BigInteger idprecio) {
		this.idprecio = idprecio;
	}

	public Articulo getIdarticulo() {
		return idarticulo;
	}

	public void setIdarticulo(Articulo idarticulo) {
		this.idarticulo = idarticulo;
	}

	public String getStgdescripcion() {
		return stgdescripcion;
	}

	public void setStgdescripcion(String stgdescripcion) {
		this.stgdescripcion = stgdescripcion;
	}

	public Timestamp getDtmfecha() {
		return dtmfecha;
	}

	public void setDtmfecha(Timestamp dtmfecha) {
		this.dtmfecha = dtmfecha;
	}

	public Double getNumprecio() {
		return numprecio;
	}

	public void setNumprecio(Double numprecio) {
		this.numprecio = numprecio;
	}

	public BigInteger getNumactivo() {
		return numactivo;
	}

	public void setNumactivo(BigInteger numactivo) {
		this.numactivo = numactivo;
	}
}
