package com.project.rappi.cubesumation.businesslogic;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.project.rappi.cubesumation.businesslogic.interfaces.ICubeSummantionSBLocal;
import com.project.rappi.cubesumation.businesslogic.persistmanaged.PersistDriverManagedSB;
import com.project.rappi.cubesumation.common.dto.*;
import com.project.rappi.cubesumation.common.enums.*;
import com.project.rappi.cubesumation.common.exceptions.BussinesLogicExceptions;
import com.project.rappi.cubesumation.modelo.*;
import com.project.rappi.cubesumation.services.TransversalUtilityService;

@Stateless
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class CubeSummantionSB implements Serializable, ICubeSummantionSBLocal {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	TransversalUtilityService transversalUtilityService;

	@EJB
	PersistDriverManagedSB<MatrixVDTO> persistDriverManagedSB;

	@Override
	public ExecuteProcessResponseDTO executeCubeSummation(ExecuteProcessDTO execute) throws BussinesLogicExceptions {
		
		MatrixV valores;
		ExecuteProcessResponseDTO response = new ExecuteProcessResponseDTO();
		Matrix matrix;
		
		if (execute.getMatrixDTO().getNumsize() == null) {
			throw new BussinesLogicExceptions(BussinesLogicErrorEnum.TAMANIO_DEL_CUBO);
		}
		if (execute.getMatrixDTO().getNumsize().intValue() <= 0) {
			throw new BussinesLogicExceptions(BussinesLogicErrorEnum.VALOR_MENOR_0);
		}		
		
		transversalUtilityService.validarSentencias(execute.getExecuteLine());		
		matrix = transversalUtilityService.idozerMappingDriven.copydata(execute.getMatrixDTO(), Matrix.class,"MatrixDTO-matrix");
		persistDriverManagedSB.persistEntity(matrix);		
		
		valore = new MatrixV(matrix,new BigInteger(String.valueOf(1)), new BigInteger(String.valueOf(1)), new BigInteger(String.valueOf(1)), BigInteger.ZERO);
		persistDriverManagedSB.persistEntity(valores);
		
		/*for (int i = 1; i <= matrix.getNumsize().intValue(); i++) {
			valores = new MatrixV(matrix,new BigInteger(String.valueOf(i)), new BigInteger(String.valueOf(i)), new BigInteger(String.valueOf(i)), BigInteger.ZERO);
			persistDriverManagedSB.persistEntity(valores);			
		}*/
		
		execute.getExecuteLine().forEach(s -> {			
			String[] comandline = s.split(" ");			
			BigInteger x = BigInteger.ZERO;
			BigInteger y = BigInteger.ZERO;
			BigInteger z = BigInteger.ZERO;
			BigInteger x2 = BigInteger.ZERO;
			BigInteger y2 = BigInteger.ZERO;
			BigInteger z2 = BigInteger.ZERO;
			BigInteger valor = BigInteger.ZERO;
			StringBuilder sql = null;
			List<ParametrosSQLDTO> parametros = null;
			ExecuteQueryResultDTO<MatrixVDTO> result =null;
			
			if (s.trim().toUpperCase().startsWith(TransactionType.UPDATE.name())){
				sql = new StringBuilder();
				x = new BigInteger(comandline[1]);
				y = new BigInteger(comandline[2]);
				z = new BigInteger(comandline[3]);
				valor = new BigInteger(comandline[4]);
				sql.append(" UPDATE MatrixV matr SET matr.numvalue = :numValSetIntro ");
				sql.append(" WHERE matr.numx = :numxIntro AND matr.numy= :numyIntro AND matr.numz= :numzIntro AND matr.matrixencid.id= :matrixIdIntro");
				parametros = new ArrayList<>();
				parametros.add(new ParametrosSQLDTO("numValSetIntro", valor));
				parametros.add(new ParametrosSQLDTO("numxIntro", x));
				parametros.add(new ParametrosSQLDTO("numyIntro", y));
				parametros.add(new ParametrosSQLDTO("numzIntro", z));
				parametros.add(new ParametrosSQLDTO("matrixIdIntro", matrix.getId()));
				persistDriverManagedSB.sqlQueryExecute(TransactionType.UPDATE,sql.toString(),parametros);
				persistDriverManagedSB.flush();
				response.getResponse().add(String.valueOf(valor.intValue()));
			}
			
			if (s.trim().toUpperCase().startsWith(TransactionType.QUERY.name())){
				x = new BigInteger(comandline[1]);
				y = new BigInteger(comandline[2]);
				z = new BigInteger(comandline[3]);
				x2 = new BigInteger(comandline[4]);
				y2 = new BigInteger(comandline[5]);
				z2 = new BigInteger(comandline[6]);
				sql = new StringBuilder();
				sql.append(" SELECT new com.project.rappi.cubesumation.common.dto.MatrixVDTO(sum(valo.numvalue)) ");
				sql.append(" FROM MatrixV valo ");
				sql.append(" INNER JOIN valo.matrixencid encma ");
				sql.append(" WHERE     ( valo.numx >= :numxIntro AND valo.numx <=:numxIntro2 ) ");
				sql.append("       AND ( valo.numy >= :numyIntro AND valo.numy <=:numyIntro2 ) ");
				sql.append("       AND ( valo.numz >= :numzIntro AND valo.numz <=:numzIntro2 ) ");
				sql.append("       AND ( encma.id = :matrixIdIntro ) ");				
				parametros = new ArrayList<>();
				parametros.add(new ParametrosSQLDTO("numxIntro", x));
				parametros.add(new ParametrosSQLDTO("numyIntro", y));
				parametros.add(new ParametrosSQLDTO("numzIntro", z));
				parametros.add(new ParametrosSQLDTO("numxIntro2", x2));
				parametros.add(new ParametrosSQLDTO("numyIntro2", y2));
				parametros.add(new ParametrosSQLDTO("numzIntro2", z2));
				parametros.add(new ParametrosSQLDTO("matrixIdIntro", matrix.getId()));
				result = persistDriverManagedSB.sqlQueryExecute(TransactionType.SELECT,sql.toString(), parametros);
				response.getResponse().add(String.valueOf(result.getResultList().get(0).getSumquery().intValue()));
			}
		});		
		return response;
	}
}